#!/usr/bin/python
# -*- coding: utf-8 -*-

# importing and intialsing libraries

import pygame
import random
import array
import time

# import pygame.locals for easier access to key co-ordinates

from pygame.locals import RLEACCEL, K_w, K_a, K_s, K_d, K_UP, K_DOWN, \
    K_LEFT, K_RIGHT, KEYDOWN, QUIT
from config import *

pygame.init()

pygame.mixer.music.load('music.mp3')

# defining the buttons


def button(
    msg,
    x,
    y,
    w,
    h,
    ic,
    ac,
    action=None,
):
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(screen, ac, (x, y, w, h))
        if click[0] == 1 and action is not None:
            action()
    else:
        pygame.draw.rect(screen, ic, (x, y, w, h))

    smallText = pygame.font.Font('freesansbold.ttf', 20)
    (TextSurf, TextRect) = text_objects(msg, smallText)
    TextRect.center = (x + w / 2, y + h / 2)
    screen.blit(TextSurf, TextRect)


# defining function to quit game

def quitgame():
    pygame.quit()
    quit()


# after the game has ended

def game_over():
    global kill2, alive2, alive1, score2, score1, time1, time2, scr1, \
        scr2, largeText
    screen.fill([165, 42, 42])
    displaymes('FINAL SCORE', 200, 325)
    displaymes('PLAYER 1:', 300, 400)
    displaymes(str(scr1), 500, 400)
    displaymes('PLAYER 2:', 300, 450)
    displaymes(str(scr2), 500, 450)
    if scr1 > scr2:
        displaymes('PLAYER 1 wins', 500, 500)
    elif scr2 > scr1:
        displaymes('PLAYER 2 wins', 500, 500)
    else:
        displaymes('GAME TIED', 100, 500)
    largeText = pygame.font.Font('freesansbold.ttf', 80)
    (TextSurf, TextRect) = text_objects('Game Over', largeText)
    TextRect.center = (1050 / 2, 100)
    screen.blit(TextSurf, TextRect)

    kill2 = 3
    alive1 = True
    alive2 = True
    score1 = 0
    score2 = 0
    scr2 = 0
    scr1 = 0
    time1 = [0, 0, 0, 0]
    time2 = [0, 0, 0, 0]
    while True:
        for event in pygame.event.get():

            # print(event)

            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        button(
            'Play Again',
            350,
            600,
            120,
            50,
            [0, 255, 0],
            [0, 200, 0],
            start,
        )
        button(
            'QUIT',
            700,
            600,
            100,
            50,
            [255, 0, 0],
            [200, 0, 0],
            quitgame,
        )
        button(
            'Help',
            950,
            600,
            100,
            50,
            [0, 0, 255],
            [0, 0, 200],
            help,
        )

        pygame.display.update()
        clock.tick(2)


# help function

def help():
    screen.fill([160, 42, 40])
    displaymes('For Player 1:', 200, 200)
    displaymes('For Player 2:', 800, 200)
    displaymes('UP', 800, 225)
    displaymes('DOWN', 800, 250)
    displaymes('LEFT', 800, 275)
    displaymes('RIGHT', 800, 300)
    displaymes(':', 900, 225)
    displaymes(':', 900, 250)
    displaymes(':', 900, 275)
    displaymes(':', 900, 300)
    displaymes('W', 1000, 225)
    displaymes('S', 1000, 250)
    displaymes('A', 1000, 275)
    displaymes('D', 1000, 300)
    displaymes('Cross the river in minimum time to win', 150, 400)
    displaymes('UP ', 200, 225)
    displaymes('DOWN ', 200, 250)
    displaymes('LEFT  ', 200, 275)
    displaymes('RIGHT', 200, 300)
    displaymes(':', 300, 225)
    displaymes(':', 300, 250)
    displaymes(':', 300, 275)
    displaymes(':', 300, 300)
    displaymes('ARROW UP', 400, 225)
    displaymes('ARROW DOWN', 400, 250)
    displaymes('ARROW LEFT', 400, 275)
    displaymes('ARROW RIGHT', 400, 300)

    largeText = pygame.font.Font('freesansbold.ttf', 80)
    (TextSurf, TextRect) = text_objects('Instructions', largeText)
    TextRect.center = (1050 / 2, 100)
    screen.blit(TextSurf, TextRect)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        button(
            'START',
            350,
            550,
            120,
            50,
            [0, 255, 0],
            [0, 200, 0],
            start,
        )
        button(
            'QUIT',
            750,
            550,
            120,
            50,
            [255, 0, 0],
            [200, 0, 0],
            quitgame,
        )
        pygame.display.update()
        clock.tick(2)


# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
# create 2 different players

class Player1(pygame.sprite.Sprite):

    def __init__(self):
        super(Player1, self).__init__()
        self.surf = pygame.image.load('person.png').convert()
        self.surf.set_colorkey((0, 0, 0), RLEACCEL)
        self.surf1 = pygame.image.load('person1.png').convert()
        self.surf1.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.surf.get_rect(topleft=(748, 960))

    # Move the sprite based on user keypresses

    def update(self, pressed_keys):

        global SCORE1, kill1, alive2, alive1, count, flag11, flag12, \
            flag13, flag14, flag15, flag16, flag17, flag18, flag19

        if pressed_keys[K_w]:

            self.rect.move_ip(0, -5)

        if pressed_keys[K_s]:

            self.rect.move_ip(0, 5)

        if pressed_keys[K_a]:

            self.rect.move_ip(-5, 0)

        if pressed_keys[K_d]:

            self.rect.move_ip(5, 0)

        # Keep player on the screen

        if self.rect.left < 0:
            self.rect.left = 0

        if self.rect.right > 1500:
            self.rect.right = 1500

        if self.rect.top <= 0:
            self.rect.bottom = 1000
            self.kill()
            alive1 = 0
            alive2 = 1
            kill1 += 1

        if self.rect.bottom >= 1000:
            self.rect.bottom = 1000

        # updating the score

        if alive1:

            # print(self.rect.bottom)

            if self.rect.bottom <= 810 and flag11 == 1:
                SCORE1 += 10
                flag11 = 0

                # print("yeah")

            if self.rect.bottom <= 760 and flag16 == 1:
                SCORE1 += 10 + kill2 * 5
                flag16 = 0
            if self.rect.bottom <= 620 and flag12 == 1:
                SCORE1 += 10
                flag12 = 0
            if self.rect.bottom <= 570 and flag17 == 1:
                SCORE1 += 10 + kill2 * 5
                flag17 = 0
            if self.rect.bottom <= 430 and flag13 == 1:
                SCORE1 += 10
                flag13 = 0
            if self.rect.bottom <= 380 and flag18 == 1:
                SCORE1 += 10 + kill2 * 5
                flag18 = 0
            if self.rect.bottom <= 240 and flag14 == 1:
                SCORE1 += 10
                flag14 = 0
            if self.rect.bottom <= 190 and flag19 == 1:
                SCORE1 += 10 + kill2 * 5
                flag19 = 0
            if self.rect.bottom <= 50 and flag15 == 1:
                SCORE1 += 10
                flag15 = 0
        if not alive1:
            self.rect.left = 748
            self.rect.top = 960
            flag11 = 1
            flag12 = 1
            flag13 = 1
            flag14 = 1
            flag15 = 1
            flag16 = 1
            flag17 = 1
            flag18 = 1
            flag19 = 1


class Player2(pygame.sprite.Sprite):

    def __init__(self):
        super(Player2, self).__init__()
        self.surf = pygame.image.load('person_1.png').convert()
        self.surf.set_colorkey((0, 0, 0), RLEACCEL)
        self.surf1 = pygame.image.load('person1.png').convert()
        self.surf1.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.surf.get_rect(topleft=(748, 0))

    # Move the sprite based on user keypresses

    def update(self, pressed_keys):

        global SCORE2, kill2, alive2, alive1, count, flag21, flag22, \
            flag23, flag24, flag25, flag26, flag27, flag28, flag29

        if pressed_keys[K_UP]:

            self.rect.move_ip(0, -5)

        if pressed_keys[K_DOWN]:

            self.rect.move_ip(0, 5)

        if pressed_keys[K_LEFT]:

            self.rect.move_ip(-5, 0)

        if pressed_keys[K_RIGHT]:

            self.rect.move_ip(5, 0)

        # Keep player on the screen

        if self.rect.left < 0:
            self.rect.left = 0

        if self.rect.right > 1500:
            self.rect.right = 1500

        if self.rect.top <= 0:
            self.rect.top = 0

        if self.rect.bottom >= 1000:
            self.rect.top = 0
            self.kill()
            alive2 = 0
            alive1 = 1
            kill2 += 1

        # updating the score

        if alive2:
            if self.rect.bottom >= 190 and flag21 == 1:
                SCORE2 += 10
                flag21 = 0
            if self.rect.bottom >= 240 and flag26 == 1:
                SCORE2 += 10 + kill2 * 5
                flag26 = 0
            if self.rect.bottom >= 380 and flag22 == 1:
                SCORE2 += 10
                flag22 = 0
            if self.rect.bottom >= 430 and flag27 == 1:
                SCORE2 += 10 + kill2 * 5
                flag27 = 0
            if self.rect.bottom >= 570 and flag23 == 1:
                SCORE2 += 10
                flag23 = 0
            if self.rect.bottom >= 620 and flag28 == 1:
                SCORE2 += 10 + kill2 * 5
                flag28 = 0
            if self.rect.bottom >= 760 and flag24 == 1:
                SCORE2 += 10
                flag24 = 0
            if self.rect.bottom >= 810 and flag29 == 1:
                SCORE2 += 10 + kill2 * 5
                flag29 = 0
            if self.rect.bottom >= 950 and flag25 == 1:
                SCORE2 += 10
                flag25 = 0
        if not alive2:
            self.rect.top = 0
            self.rect.left = 748
            flag21 = 1
            flag22 = 1
            flag23 = 1
            flag24 = 1
            flag25 = 1
            flag26 = 1
            flag27 = 1
            flag28 = 1
            flag29 = 1


# Define the enemy object by extending pygame.sprite.Sprite
# The surface you draw on the screen is now an attribute of 'enemy'

# define enemies

class Enemy1(pygame.sprite.Sprite):

    def __init__(self):

        global v1
        super(Enemy1, self).__init__()

        self.surf = pygame.image.load('boat1.png').convert()
        self.surf.set_colorkey((0, 162, 232), RLEACCEL)

        # var = 190*random.randint(0,4)

        self.rect = self.surf.get_rect(
            center=(
                random.randint(
                    0, 1500), random.randint(
                    50 + 19 + 190 * 0, 190 - 19 + 190 * 0)))

        self.speed = 8 + v1

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen

    def update(self):
        self.rect.move_ip(self.speed, 0)

        if self.rect.left > 1500:
            self.rect.left = 0


class Enemy2(pygame.sprite.Sprite):

    def __init__(self):

        global v1
        super(Enemy2, self).__init__()

        self.surf = pygame.image.load('boat1.png').convert()
        self.surf.set_colorkey((0, 162, 232), RLEACCEL)

        # var = 190*random.randint(0,4)

        self.rect = self.surf.get_rect(
            center=(
                random.randint(
                    0, 1500), random.randint(
                    50 + 19 + 190 * 1, 190 - 19 + 190 * 1)))

        self.speed = 8 + v1

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen

    def update(self):
        self.rect.move_ip(self.speed, 0)

        if self.rect.left > 1500:
            self.rect.left = 0


class Enemy3(pygame.sprite.Sprite):

    def __init__(self):

        global v1
        super(Enemy3, self).__init__()

        self.surf = pygame.image.load('boat1.png').convert()
        self.surf.set_colorkey((0, 162, 232), RLEACCEL)

        # var = 190*random.randint(0,4)

        self.rect = self.surf.get_rect(
            center=(
                random.randint(
                    0, 1500), random.randint(
                    50 + 19 + 190 * 2, 190 - 19 + 190 * 2)))

        self.speed = 8 + v1

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen

    def update(self):
        self.rect.move_ip(self.speed, 0)

        if self.rect.left > 1500:
            self.rect.left = 0


class Enemy4(pygame.sprite.Sprite):

    def __init__(self):

        global v1
        super(Enemy4, self).__init__()

        self.surf = pygame.image.load('boat1.png').convert()
        self.surf.set_colorkey((0, 162, 232), RLEACCEL)

        # var = 190*random.randint(0,4)

        self.rect = self.surf.get_rect(
            center=(
                random.randint(
                    0, 1500), random.randint(
                    50 + 19 + 190 * 3, 190 - 19 + 190 * 3)))

        self.speed = 8 + v1

        # print(self.speed)

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen

    def update(self):
        self.rect.move_ip(self.speed, 0)

        if self.rect.left > 1500:
            self.rect.left = 0


class Enemy5(pygame.sprite.Sprite):

    def __init__(self):

        global v1
        super(Enemy5, self).__init__()

        self.surf = pygame.image.load('boat1.png').convert()
        self.surf.set_colorkey((0, 162, 232), RLEACCEL)

        # var = 190*random.randint(0,4)

        self.rect = self.surf.get_rect(center=(
            random.randint(-250, 1000), random.randint(50 + 19 + 190 * 4, 190 - 19 + 190 * 4)))

        self.speed = 8 + v1

    # Move the sprite based on speed
    # Remove the sprite when it passes the left edge of the screen

    def update(self):
        self.rect.move_ip(self.speed, 0)

        if self.rect.left > 1500:
            self.rect.left = 0


# make obstacles

for i in range(1, kill2 + 3):

    class Obstacle1(pygame.sprite.Sprite):

        def __init__(self):

            super(Obstacle1, self).__init__()

            self.surf = pygame.image.load('obs.png').convert()
            self.surf.set_colorkey((255, 255, 255), RLEACCEL)
            self.rect = self.surf.get_rect(
                center=(random.randint(100, 1400), 190 + 21))


for i in range(1, kill2 + 3):

    class Obstacle2(pygame.sprite.Sprite):

        def __init__(self):

            super(Obstacle2, self).__init__()

            self.surf = pygame.image.load('obs.png').convert()
            self.surf.set_colorkey((255, 255, 255), RLEACCEL)
            self.rect = self.surf.get_rect(
                center=(random.randint(100, 1400), 190 * 2 + 21))


for i in range(1, kill2 + 3):

    class Obstacle3(pygame.sprite.Sprite):

        def __init__(self):

            super(Obstacle3, self).__init__()

            self.surf = pygame.image.load('obs.png').convert()
            self.surf.set_colorkey((255, 255, 255), RLEACCEL)
            self.rect = self.surf.get_rect(
                center=(random.randint(100, 1400), 190 * 3 + 21))


for i in range(1, kill2 + 3):

    class Obstacle4(pygame.sprite.Sprite):

        def __init__(self):

            super(Obstacle4, self).__init__()

            self.surf = pygame.image.load('obs.png').convert()
            self.surf.set_colorkey((255, 255, 255), RLEACCEL)
            self.rect = self.surf.get_rect(
                center=(random.randint(100, 1400), 190 * 4 + 21))


# for displaying score

class Score1(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.font = pygame.font.Font(None, 30)
        self.color = pygame.Color('black')
        self.lastscore = -1
        self.update()
        self.rect = pygame.draw.rect(screen, (100, 0, 200), (20, 1005,
                                                             40, 10), 10)

    def update(self):
        if SCORE1 != self.lastscore:
            self.lastscore = SCORE1
            msg = 'Player1: %ld' % SCORE1
            self.image = self.font.render(msg, 0, self.color)


class Score2(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.font = pygame.font.Font(None, 30)
        self.color = pygame.Color('black')
        self.lastscore = -1
        self.update()
        self.rect = pygame.draw.rect(screen, (100, 0, 200), (1250,
                                                             1005, 40, 10), 10)

    def update(self):
        if SCORE2 != self.lastscore:
            self.lastscore = SCORE2
            msg = 'Player2: %lu' % SCORE2
            self.image = self.font.render(msg, 0, self.color)


# setting window size

screen = pygame.display.set_mode([1500, 1030])

# Create a custom event for adding a new enemy
# ADDENEMY = pygame.USEREVENT + 1
# pygame.time.set_timer(ADDENEMY, 1000)

# Create the player

player1 = Player1()
player2 = Player2()
Scor1 = Score1()
Scor2 = Score2()
enemy1 = Enemy1()
enemy2 = Enemy2()
enemy3 = Enemy3()
enemy4 = Enemy4()
enemy5 = Enemy5()

# Create groups to hold enemy sprites and all sprites
# - enemies is used for collision detection and position updates
# - all_sprites is used for rendering

enemies = pygame.sprite.Group()
all_sprites = pygame.sprite.Group()
all_sprites.add(player1)
all_sprites.add(player2)

# Setup the clock for a decent framerate

clock = pygame.time.Clock()


# add new enemies

def add():
    new_enemy = Enemy1()
    enemies.add(new_enemy)
    all_sprites.add(new_enemy)
    new_enemy = Enemy2()
    enemies.add(new_enemy)
    all_sprites.add(new_enemy)
    new_enemy = Enemy3()
    enemies.add(new_enemy)
    all_sprites.add(new_enemy)
    new_enemy = Enemy4()
    enemies.add(new_enemy)
    all_sprites.add(new_enemy)
    new_enemy = Enemy5()
    enemies.add(new_enemy)
    all_sprites.add(new_enemy)
    for i in range(1, kill2 + 3):
        new_enemy = Obstacle1()
        enemies.add(new_enemy)
        all_sprites.add(new_enemy)
    for i in range(1, kill2 + 3):
        new_enemy = Obstacle2()
        enemies.add(new_enemy)
        all_sprites.add(new_enemy)
    for i in range(1, kill2 + 3):
        new_enemy = Obstacle3()
        enemies.add(new_enemy)
        all_sprites.add(new_enemy)
    for i in range(1, kill2 + 3):
        new_enemy = Obstacle4()
        enemies.add(new_enemy)
        all_sprites.add(new_enemy)


# display messages

def intro():
    init = 1
    while init is 1:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        screen.fill([160, 82, 45])
        button(
            'START',
            720,
            300,
            100,
            60,
            [0, 255, 0],
            [0, 200, 0],
            start,
        )
        button(
            'QUIT',
            720,
            500,
            100,
            50,
            [255, 0, 0],
            [200, 0, 0],
            quitgame,
        )
        button(
            'Help',
            720,
            700,
            100,
            50,
            [0, 0, 255],
            [0, 0, 200],
            help,
        )
        pygame.display.update()
        clock.tick(2)


def start():

    # Run until the user asks to quit

    pygame.mixer.music.play(-1)
    running = True
    global kill2, kill1, prev_level, prev_time, alive1, alive2, time1, \
        time2, flag, v1
    while running and kill2 <= 3:

        global t1, t2, scr1, scr2
        if pygame.time.get_ticks() - prev_time > 1000:
            prev_time = pygame.time.get_ticks()
            if alive1:
                time1[kill2] += 1
            if alive2:
                time2[kill2] += 1
        t1 = time1[0] + time1[1] + time1[2] + time1[3]
        t2 = time2[0] + time2[1] + time2[2] + time2[3]
        scr1 = SCORE1 - t1
        scr2 = SCORE2 - t2

        # print(time)
        # End the program if user clicks the window close button

        for event in pygame.event.get():

            if event.type == pygame.QUIT:

                running = False

            # elif event.type == ADDENEMY:
            # Add a new enemy?
            # Create the new enemy and add it to sprite groups

        # Get the set of keys pressed and check for user input

        pressed_keys = pygame.key.get_pressed()

        # Update the player sprite based on user keypresses
        # if alive1 == 1:

        player1.update(pressed_keys)
        player2.update(pressed_keys)

        # Update enemy position

        enemies.update()

        # update score

        if alive1 == 1:
            Scor1.update()
        if alive2 == 1:
            Scor2.update()

        screen.fill([0, 180, 220])

        lines = 6

        while lines > 0:
            y = 190 * (6 - lines)
            pygame.draw.line(screen, (150, 100, 0), (0, 25 + y), (1500,
                                                                  25 + y), 50)
            lines = lines - 1
        pygame.draw.line(screen, (0, 0, 255), (0, 1015), (1500, 1015),
                         30)

        # Draw all sprites

        for entity in enemies:
            screen.blit(entity.surf, entity.rect)

        # Draw the player on screen

        if alive1 == 1:
            screen.blit(player1.surf, player1.rect)
        if alive2 == 1:
            screen.blit(player2.surf, player2.rect)

        # print score and time

        screen.blit(Scor1.image, Scor1.rect)
        screen.blit(Scor2.image, Scor2.rect)
        dis = 'Level: ' + str(kill2 + 1)
        tp1 = '     Time P1: ' + str(time1[kill2])
        tp2 = '     Time P2: ' + str(time2[kill2])
        displaymes(str(dis), 725, 1000)
        displaymes(str(tp1), 300, 1000)
        displaymes(str(tp2), 1000, 1000)

        # Check if any enemies have collided with the player

        if pygame.sprite.spritecollideany(player1, enemies):

            # If so, then remove the player and stop the loop

            pygame.sprite.Sprite.kill(player1)
            alive1 = 0
            alive2 = 1
            kill1 += 1

        if pygame.sprite.spritecollideany(player2, enemies):
            pygame.sprite.Sprite.kill(player2)
            alive2 = 0
            alive1 = 1
            kill2 += 1

        # increase speed as level increases

        if kill1 == kill2 and flag == 1:
            add()
            flag = 0
        if kill2 != prev_level:
            add()
            v1 += 5

        # Flip the display

        pygame.display.update()

        # Ensure program maintains a rate of 30 frames per second

        clock.tick(40)

        prev_level = kill2
        if kill2 is 4:
            game_over()


intro()
start()

# if SCORE1 > SCORE2:
#     var = "Player 1  WINS"
# Quits the program

pygame.quit()
quit()
